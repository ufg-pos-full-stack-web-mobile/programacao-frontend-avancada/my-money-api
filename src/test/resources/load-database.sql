INSERT INTO conta(nome, saldo) VALUES ('Conta 1', 100.0);
INSERT INTO conta(nome, saldo) VALUES ('Conta 2', 150.0);
INSERT INTO conta(nome, saldo) VALUES ('Conta 3', 255.0);
INSERT INTO conta(nome, saldo) VALUES ('Conta sem relacionamento', 10.5);

INSERT INTO lancamento(data, valor, tipo_lancamento, id_conta) VALUES (DATE '2017-12-05', 15.5, 'SAIDA', 2);
INSERT INTO lancamento(data, valor, tipo_lancamento, id_conta) VALUES (DATE '2017-12-07', 100.0, 'ENTRADA', 1);
INSERT INTO lancamento(data, valor, tipo_lancamento, id_conta) VALUES (DATE '2017-12-10', 15.25, 'SAIDA', 3);