package br.ufg.pos.fswb.mymoneyapi.lancamento.resource;

import br.ufg.pos.fswb.mymoneyapi.MyMoneyApiApplicationTests;
import br.ufg.pos.fswb.mymoneyapi.base.util.DataUtil;
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.time.Month;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class LancamentoResourceTest extends MyMoneyApiApplicationTests {

    @Test
    public void deve_ser_possivel_recuperar_todos_os_lancamentos_da_aplicacao() throws Exception {
        given()
                .get("/lancamentos")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .body("codigo", contains(1, 2, 3),
                            "dia", contains(DataUtil.getLongMili(2017, Month.DECEMBER, 5),
                                    DataUtil.getLongMili(2017, Month.DECEMBER, 7),
                                    DataUtil.getLongMili(2017, Month.DECEMBER, 10)),
                            "valor", contains(15.5f, 100.0f, 15.25f),
                            "conta", contains("Conta 2", "Conta 1", "Conta 3"),
                            "tipo", contains("SAIDA", "ENTRADA", "SAIDA"));
    }

    @Test
    public void deve_ser_possivel_criar_novo_lancamento() throws Exception {
        JSONObject object = new JSONObject();
        object.put("dia", DataUtil.getLongMili(2018, Month.APRIL, 7));
        object.put("valor", 20.50);
        object.put("conta", 3);
        object.put("tipo", "SAIDA");

        given()
                .request()
                    .header("Accept", ContentType.ANY)
                    .header("Content-type", ContentType.JSON)
                    .body(object.toString())
                .post("/lancamentos")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.CREATED.value())
                    .header("Location", equalTo("http://localhost:" + porta + "/lancamentos/4"))
                    .body("codigo", equalTo(4),
                            "dia", equalTo(DataUtil.getLongMili(2018, Month.APRIL, 7)),
                            "valor", equalTo(20.5f),
                            "conta", equalTo("Conta 3"),
                            "tipo", equalTo("SAIDA"));

    }

    @Test
    public void deve_atualizar_saldo_da_conta_ao_salvar_novo_lancamento() throws Exception {
        JSONObject object = new JSONObject();
        object.put("dia", DataUtil.getLongMili(2018, Month.APRIL, 7));
        object.put("valor", 20.50);
        object.put("conta", 3);
        object.put("tipo", "SAIDA");

        given()
                .request()
                    .header("Accept", ContentType.ANY)
                    .header("Content-type", ContentType.JSON)
                    .body(object.toString())
                .post("/lancamentos")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.CREATED.value())
                    .header("Location", equalTo("http://localhost:" + porta + "/lancamentos/4"))
                    .body("codigo", equalTo(4),
                            "dia", equalTo(DataUtil.getLongMili(2018, Month.APRIL, 7)),
                            "valor", equalTo(20.5f),
                            "conta", equalTo("Conta 3"),
                            "tipo", equalTo("SAIDA"));

        given()
                .get("/contas/3")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .body("codigo", equalTo(3),
                            "nome", equalTo("Conta 3"),
                            "saldo", equalTo(234.5f));
    }

    @Test
    public void deve_filtrar_lancamentos_utilizando_alguns_parametros_de_busca() throws Exception {
        JSONObject object = new JSONObject();
        object.put("diaInicial", DataUtil.getLongMili(2017, Month.DECEMBER, 6));
        object.put("diaFinal", DataUtil.getLongMili(2017, Month.DECEMBER, 12));
        object.put("valorInicial", 20.0);
        object.put("valorFinal", 200.0);
        object.put("conta", 1L);

        given()
                .request()
                    .header("Accept", ContentType.ANY)
                    .header("Content-type", ContentType.JSON)
                    .body(object.toString())
                .post("/lancamentos/filtro")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .body("codigo", contains(2),
                            "valor", contains(100.0f),
                            "dia", contains(DataUtil.getLongMili(2017, Month.DECEMBER, 7)),
                            "tipo", contains("ENTRADA"),
                            "conta", contains("Conta 1"));
    }
}
