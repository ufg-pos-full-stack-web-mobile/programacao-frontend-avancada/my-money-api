package br.ufg.pos.fswb.mymoneyapi.conta.resource;

import br.ufg.pos.fswb.mymoneyapi.MyMoneyApiApplicationTests;
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class ContaResourceTest extends MyMoneyApiApplicationTests {

    @Test
    public void deve_retornar_todas_as_contas_pre_carregadas_na_aplicacao() throws Exception {
        given()
                .get("/contas")
                .then()
                    .statusCode(HttpStatus.OK.value())
                .and()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .body("codigo", containsInAnyOrder(1, 2, 3, 4),
                            "nome", containsInAnyOrder("Conta 1", "Conta 2", "Conta 3", "Conta sem relacionamento"),
                            "saldo", containsInAnyOrder(100.0f, 150.0f, 255.0f, 10.5f));
    }

    @Test
    public void deve_salvar_nova_conta_na_aplicacao() throws Exception {
        JSONObject object = new JSONObject();
        object.put("nome", "Conta nova");
        object.put("saldo", 2030.5);

        given()
                .request()
                    .header("Accept", ContentType.ANY)
                    .header("Content-type", ContentType.JSON)
                    .body(object.toString())
                .post("/contas")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.CREATED.value())
                    .header("Location", equalTo("http://localhost:" + porta + "/contas/5"))
                    .body("codigo", equalTo(5),
                            "nome", equalTo("Conta nova"),
                            "saldo", equalTo(2030.5f));
    }

    @Test
    public void deve_ser_possivel_buscar_conta_por_identificador() throws Exception {
        given()
                .get("/contas/2")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .body("codigo", equalTo(2),
                            "nome", equalTo("Conta 2"),
                            "saldo", equalTo(150.0f));
    }

    @Test
    public void nao_deve_ser_possivel_buscar_conta_por_identificador_inexistente() throws Exception {
        given()
                .get("/contas/45")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .body("[0].titulo", equalTo("Recurso inexistente"),
                            "[0].detalhe", equalTo("Não foi possível encontrar o recurso conta com o identificador '45'"),
                            "[0].source", equalTo("/contas/45"));
    }

    @Test
    public void deve_ser_possivel_excluir_conta_pelo_identificador() throws Exception {
        given()
                .delete("/contas/4")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .body("codigo", equalTo(4),
                            "nome", equalTo("Conta sem relacionamento"),
                            "saldo", equalTo(10.5f));

        /* Procuro para saber se foi realmente excluído */
        given()
                .get("/contas/4")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .body("[0].titulo", equalTo("Recurso inexistente"),
                        "[0].detalhe", equalTo("Não foi possível encontrar o recurso conta com o identificador '4'"),
                        "[0].source", equalTo("/contas/4"));
    }

    @Test
    public void deve_tratar_requisicao_options() throws Exception {
        given()
                .options("/contas")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value());
    }
}
