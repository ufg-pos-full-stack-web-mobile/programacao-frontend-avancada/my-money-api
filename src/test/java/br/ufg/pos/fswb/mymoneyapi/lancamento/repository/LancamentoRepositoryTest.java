package br.ufg.pos.fswb.mymoneyapi.lancamento.repository;

import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Sql(value = "/load-database.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/clean-database.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application-test.properties")
public class LancamentoRepositoryTest {

    @Autowired
    private LancamentoRepository sut;

    @Test
    public void deve_filtrar_lancamento_utilizando_data_inicial_como_parametro_de_busca() throws Exception {
        final LancamentoFiltro filtro = new LancamentoFiltro();
        filtro.setDataInicial(LocalDate.of(2017, Month.DECEMBER, 6));

        List<Lancamento> lancamentos = sut.filtrar(filtro);

        final Lancamento lancamento = new Lancamento();
        lancamento.setId(2L);

        final Lancamento lancamento2 = new Lancamento();
        lancamento2.setId(3L);

        assertThat(lancamentos).containsOnly(lancamento, lancamento2);
    }

    @Test
    public void deve_filtrar_lancamento_utilizando_data_final_como_parametro() throws Exception {
        final LancamentoFiltro filtro = new LancamentoFiltro();
        filtro.setDataFinal(LocalDate.of(2017, Month.DECEMBER, 6));

        List<Lancamento> lancamentos = sut.filtrar(filtro);

        final Lancamento lancamento = new Lancamento();
        lancamento.setId(1L);

        assertThat(lancamentos).containsOnly(lancamento);
    }

    @Test
    public void deve_filtrar_lancamento_por_valor_maior_que_recebido_no_filtro() throws Exception {
        final LancamentoFiltro filtro = new LancamentoFiltro();
        filtro.setValorInicial(BigDecimal.valueOf(55));

        List<Lancamento> lancamentos = sut.filtrar(filtro);

        final Lancamento lancamento = new Lancamento();
        lancamento.setId(2L);

        assertThat(lancamentos).containsOnly(lancamento);
    }

    @Test
    public void deve_filtrar_lancamento_por_valor_menor_que_recebido_no_filtro() throws Exception {
        final LancamentoFiltro filtro = new LancamentoFiltro();
        filtro.setValorFinal(BigDecimal.valueOf(55));

        List<Lancamento> lancamentos = sut.filtrar(filtro);

        final Lancamento lancamento = new Lancamento();
        lancamento.setId(1L);

        final Lancamento lancamento2 = new Lancamento();
        lancamento2.setId(3L);

        assertThat(lancamentos).containsOnly(lancamento, lancamento2);
    }

    @Test
    public void deve_filtrar_lancamento_por_conta() throws Exception {
        final Conta conta = new Conta();
        conta.setId(2L);

        final LancamentoFiltro filtro = new LancamentoFiltro();
        filtro.setConta(conta);

        List<Lancamento> lancamentos = sut.filtrar(filtro);

        final Lancamento lancamento = new Lancamento();
        lancamento.setId(1L);

        assertThat(lancamentos).containsOnly(lancamento);
    }
}