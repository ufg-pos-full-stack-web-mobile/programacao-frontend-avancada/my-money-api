package br.ufg.pos.fswb.mymoneyapi.conta.services;

import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.NaoEncontradoException;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.conta.repository.ContaRepository;
import br.ufg.pos.fswb.mymoneyapi.conta.services.impl.ContaServiceImpl;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.TipoLancamento;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ContaServiceTest {

    private static final long ID = 5L;
    private static final double SALDO = 254.6;
    private static final double VALOR_LANCAMENTO = 50.5;
    private static final double SALDO_FINAL = SALDO - VALOR_LANCAMENTO;
    private static final double SALDO_FINAL_SOMADO = SALDO + VALOR_LANCAMENTO;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @MockBean
    private ContaRepository repositoryMock;

    private ContaService sut;

    private Conta conta;

    @Before
    public void setUp() throws Exception {
        conta = new Conta();
        conta.setId(ID);
        conta.setSaldo(BigDecimal.valueOf(SALDO));

        sut = new ContaServiceImpl(repositoryMock);

        when(repositoryMock.findById(ID)).thenReturn(Optional.of(conta));
    }

    @Test
    public void deve_ser_possivel_buscar_conta_pelo_identificador() throws Exception {
        Conta conta = sut.buscarPorIdentificador(ID);

        assertThat(conta).isNotNull();
    }

    @Test
    public void deve_retornar_excecao_de_conta_nao_encontrada_quando_buscar_por_id_inexistente() throws Exception {
        when(repositoryMock.findById(ID)).thenReturn(Optional.empty());

        expectedException.expect(NaoEncontradoException.class);
        expectedException.expect(hasProperty("motivo", is("recurso.nao.encontrado")));
        expectedException.expect(hasProperty("detalhe", is("recurso.nao.encontrado.detalhe")));
        expectedException.expect(hasProperty("params", arrayContaining(ID)));

        sut.buscarPorIdentificador(ID);
    }

    @Test
    public void deve_ser_possivel_atualizar_saldo_da_conta_atraves_de_um_lancamento() throws Exception {
        final Lancamento lancamento = new Lancamento();
        lancamento.setValor(BigDecimal.valueOf(VALOR_LANCAMENTO));
        lancamento.setTipoLancamento(TipoLancamento.SAIDA);
        lancamento.setConta(conta);

        Conta conta = sut.atualizarSaldo(lancamento);

        assertThat(conta.getSaldo()).isEqualTo(BigDecimal.valueOf(SALDO_FINAL));
    }

    @Test
    public void deve_ser_possivel_atualizar_saldo_da_conta_atraves_de_um_lancamento_levando_em_consideracao_tipo_do_lancamento() throws Exception {
        final Lancamento lancamento = new Lancamento();
        lancamento.setValor(BigDecimal.valueOf(VALOR_LANCAMENTO));
        lancamento.setTipoLancamento(TipoLancamento.ENTRADA);
        lancamento.setConta(conta);

        Conta conta = sut.atualizarSaldo(lancamento);

        assertThat(conta.getSaldo()).isEqualTo(BigDecimal.valueOf(SALDO_FINAL_SOMADO));
    }


}