package br.ufg.pos.fswb.mymoneyapi.lancamento.service;

import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.conta.services.ContaService;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.LancamentoRepository;
import br.ufg.pos.fswb.mymoneyapi.lancamento.service.impl.LancamentoServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LancamentoServiceTest {

    private static final long ID_CONTA = 3L;
    private static final String NOME_CONTA = "Nome conta";
    private static final double VALOR_LANCAMENTO = 15.5;

    @MockBean
    private LancamentoRepository repositoryMock;

    @MockBean
    private ContaService contaServiceMock;

    private LancamentoService sut;

    private Lancamento lancamento;
    private Conta conta;

    @Before
    public void setUp() throws Exception {
        sut = new LancamentoServiceImpl(repositoryMock, contaServiceMock);

        conta = new Conta();
        conta.setId(ID_CONTA);
        conta.setNome(NOME_CONTA);

        lancamento = new Lancamento();
        lancamento.setConta(conta);
        lancamento.setValor(BigDecimal.valueOf(VALOR_LANCAMENTO));

        when(contaServiceMock.buscarPorIdentificador(ID_CONTA)).thenReturn(conta);
        when(repositoryMock.save(lancamento)).thenReturn(lancamento);
    }

    @Test
    public void deve_buscar_dados_da_conta_ao_salvar_novo_lancamento() throws Exception {
        Conta contaSemNome = new Conta();
        contaSemNome.setId(ID_CONTA);
        lancamento.setConta(contaSemNome);

        Lancamento retornado = sut.salvar(lancamento);

        assertThat(retornado).isNotNull();
        assertThat(retornado.getConta()).isNotNull();
        assertThat(retornado.getConta().getId()).isEqualTo(ID_CONTA);
        assertThat(retornado.getConta().getNome()).isEqualTo(NOME_CONTA);
    }

    @Test
    public void deve_atualizar_saldo_da_conta_ao_lancar_novo_lancamento() throws Exception {
        sut.salvar(lancamento);

        verify(contaServiceMock).atualizarSaldo(lancamento);
    }
}