package br.ufg.pos.fswb.mymoneyapi.base.resource;

import br.ufg.pos.fswb.mymoneyapi.MyMoneyApiApplicationTests;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CORSTest extends MyMoneyApiApplicationTests {

    @Test
    public void deve_retornar_headers_do_cors_ao_fazer_requisicoes_options_na_aplicacao() throws Exception {
        given()
                .options("/contas")
                .then()
                    .log().headers()
                .and()
                    .log().body()
                .and()
                    .statusCode(HttpStatus.OK.value())
                    .header("Access-Control-Allow-Origin", equalTo("http://localhost:9000"))
                    .header("Access-Control-Allow-Methods", equalTo("POST, GET, OPTIONS, DELETE"))
                    .header("Access-Control-Allow-Headers", equalTo("Content-Type, Accept"))
                    .header("Access-Control-Max-Age", equalTo("3600"));
    }
}
