package br.ufg.pos.fswb.mymoneyapi.base.resource.events;

import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;

public class RecursoCriadoEvent extends ApplicationEvent {
    private final HttpServletResponse response;
    private final Object identificador;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public RecursoCriadoEvent(Object source, HttpServletResponse response, Object identificador) {
        super(source);
        this.response = response;
        this.identificador = identificador;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public Object getIdentificador() {
        return identificador;
    }
}
