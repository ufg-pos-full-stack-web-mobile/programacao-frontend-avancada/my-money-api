package br.ufg.pos.fswb.mymoneyapi.lancamento.resource;

import br.ufg.pos.fswb.mymoneyapi.base.resource.events.RecursoCriadoEvent;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto.LancamentoDTO;
import br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto.LancamentoFiltroDTO;
import br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto.LancamentoNovoDTO;
import br.ufg.pos.fswb.mymoneyapi.lancamento.service.LancamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/lancamentos")
public class LancamentoResource {

    @Autowired
    private LancamentoService lancamentoService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @GetMapping
    public List<LancamentoDTO> recuperarTodos() {
        final List<LancamentoDTO> lancamentos = new ArrayList<>();
        lancamentoService.recuperarTodos().forEach(lancamento -> lancamentos.add(LancamentoDTO.LancamentoDTOTransformer.toDto(lancamento)));
        return lancamentos;
    }

    @PostMapping
    public ResponseEntity<LancamentoDTO> salvarNovo(@RequestBody LancamentoNovoDTO lancamentoNovoDTO, HttpServletResponse response) {
        Lancamento lancamento = LancamentoNovoDTO.LancamentoNovoDTOTransformer.toDto(lancamentoNovoDTO);
        lancamento = lancamentoService.salvar(lancamento);
        eventPublisher.publishEvent(new RecursoCriadoEvent(this, response, lancamento.getId()));
        return new ResponseEntity<>(LancamentoDTO.LancamentoDTOTransformer.toDto(lancamento), HttpStatus.CREATED);
    }

    @PostMapping("/filtro")
    public ResponseEntity<List<LancamentoDTO>> filtrar(@RequestBody LancamentoFiltroDTO filtro) {
        final List<Lancamento> lancamentos = lancamentoService.filtrar(LancamentoFiltroDTO.LancamentoFiltroDTOTransformer.toFilter(filtro));
        final List<LancamentoDTO> dtos = new ArrayList<>();
        lancamentos.forEach(lancamento -> dtos.add(LancamentoDTO.LancamentoDTOTransformer.toDto(lancamento)));
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

}
