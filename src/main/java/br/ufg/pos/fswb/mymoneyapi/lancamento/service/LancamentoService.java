package br.ufg.pos.fswb.mymoneyapi.lancamento.service;

import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;

import java.util.List;

public interface LancamentoService {
    List<Lancamento> recuperarTodos();

    Lancamento salvar(Lancamento lancamento);

    List<Lancamento> filtrar(LancamentoFiltro lancamentoFiltro);
}
