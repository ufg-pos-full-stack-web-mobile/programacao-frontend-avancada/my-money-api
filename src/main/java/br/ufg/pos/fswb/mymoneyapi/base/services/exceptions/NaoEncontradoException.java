package br.ufg.pos.fswb.mymoneyapi.base.services.exceptions;

public class NaoEncontradoException extends AppGenericException {

    private static final String MOTIVO = "recurso.nao.encontrado";
    private static final String DETALHE = "recurso.nao.encontrado.detalhe";

    public NaoEncontradoException(Object identificador) {
        super(MOTIVO, DETALHE, identificador);
    }
}
