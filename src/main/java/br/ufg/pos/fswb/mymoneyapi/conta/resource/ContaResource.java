package br.ufg.pos.fswb.mymoneyapi.conta.resource;

import br.ufg.pos.fswb.mymoneyapi.base.resource.events.RecursoCriadoEvent;
import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.AppGenericException;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.conta.resource.dto.ContaDTO;
import br.ufg.pos.fswb.mymoneyapi.conta.services.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/contas")
public class ContaResource {

    @Autowired
    private ContaService contaService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @GetMapping
    public List<ContaDTO> recuperarTodas() {
        final List<ContaDTO> contas = new ArrayList<>();
        contaService.recuperarTodas().forEach(conta -> contas.add(ContaDTO.ContaDTOTransformer.toDto(conta)));
        return contas;
    }

    @PostMapping
    public ResponseEntity<ContaDTO> criarNova(@RequestBody ContaDTO contaDto, HttpServletResponse response) {
        Conta conta = ContaDTO.ContaDTOTransformer.toEntity(contaDto);
        conta = contaService.salvar(conta);
        publisher.publishEvent(new RecursoCriadoEvent(this, response, conta.getId()));
        return new ResponseEntity<>(ContaDTO.ContaDTOTransformer.toDto(conta), HttpStatus.CREATED);
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<ContaDTO> buscarPorId(@PathVariable("codigo") Long codigo) throws AppGenericException {
        final Conta conta = contaService.buscarPorIdentificador(codigo);
        return new ResponseEntity<>(ContaDTO.ContaDTOTransformer.toDto(conta), HttpStatus.OK);
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity<ContaDTO> excluir(@PathVariable("codigo") Long codigo) {
        final Conta conta = contaService.excluirPorCodigo(codigo);
        return new ResponseEntity<>(ContaDTO.ContaDTOTransformer.toDto(conta), HttpStatus.OK);
    }
}
