package br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter;

import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LancamentoFiltro {
    private LocalDate dataInicial;
    private LocalDate dataFinal;
    private BigDecimal valorInicial;
    private BigDecimal valorFinal;
    private Conta conta;

    public void setDataInicial(LocalDate dataInicial) {
        this.dataInicial = dataInicial;
    }

    public LocalDate getDataInicial() {
        return dataInicial;
    }

    public void setDataFinal(LocalDate dataFinal) {
        this.dataFinal = dataFinal;
    }

    public LocalDate getDataFinal() {
        return dataFinal;
    }

    public void setValorInicial(BigDecimal valorInicial) {
        this.valorInicial = valorInicial;
    }

    public BigDecimal getValorInicial() {
        return valorInicial;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Conta getConta() {
        return conta;
    }
}
