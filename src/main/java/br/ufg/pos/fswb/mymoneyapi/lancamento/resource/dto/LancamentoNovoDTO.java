package br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto;

import br.ufg.pos.fswb.mymoneyapi.base.util.DataUtil;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.TipoLancamento;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LancamentoNovoDTO {

    private Long dia;
    private Double valor;
    private Long conta;
    private String tipo;

    public Long getDia() {
        return dia;
    }

    public void setDia(Long dia) {
        this.dia = dia;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getConta() {
        return conta;
    }

    public void setConta(Long conta) {
        this.conta = conta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public static class LancamentoNovoDTOTransformer {
        public static Lancamento toDto(LancamentoNovoDTO dto) {
            final Lancamento lancamento = new Lancamento();

            lancamento.setDia(DataUtil.fromTimestamp(dto.getDia()));

            Conta conta  = new Conta();
            conta.setId(dto.getConta());
            lancamento.setConta(conta);

            lancamento.setTipoLancamento(TipoLancamento.valueOf(dto.tipo));
            lancamento.setValor(BigDecimal.valueOf(dto.getValor()));
            return lancamento;
        }
    }
}
