package br.ufg.pos.fswb.mymoneyapi.lancamento.repository.helper;

import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.LancamentoRepositoryQueries;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LancamentoRepositoryQueriesImpl implements LancamentoRepositoryQueries {

    @PersistenceContext
    protected EntityManager manager;

    @Override
    public List<Lancamento> filtrar(LancamentoFiltro filtro) {
        final StringBuilder sql = new StringBuilder();
        final Map<String, Object> params = new HashMap<>();

        sql.append(" SELECT DISTINCT bean FROM Lancamento bean WHERE 1=1 ");

        if (filtro.getDataInicial() != null) {
            sql.append(" AND bean.dia >= :diaInicial ");
            params.put("diaInicial", filtro.getDataInicial());
        }

        if (filtro.getDataFinal() != null) {
            sql.append(" AND bean.dia <= :diaFinal ");
            params.put("diaFinal", filtro.getDataFinal());
        }

        if (filtro.getValorInicial() != null) {
            sql.append(" AND bean.valor >= :valorInicial ");
            params.put("valorInicial", filtro.getValorInicial());
        }

        if (filtro.getValorFinal() != null) {
            sql.append(" AND bean.valor <= :valorFinal ");
            params.put("valorFinal", filtro.getValorFinal());
        }

        if (filtro.getConta() != null) {
            sql.append(" AND bean.conta = :conta ");
            params.put("conta", filtro.getConta());
        }

        Query query = manager.createQuery(sql.toString(), Lancamento.class);

        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        return query.getResultList();
    }
}
