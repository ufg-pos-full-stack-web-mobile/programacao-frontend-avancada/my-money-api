package br.ufg.pos.fswb.mymoneyapi.base.resource.events.listeners;

import br.ufg.pos.fswb.mymoneyapi.base.resource.events.RecursoCriadoEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent> {

    @Override
    public void onApplicationEvent(RecursoCriadoEvent event) {
        HttpServletResponse response = event.getResponse();
        Object identificador = event.getIdentificador();

        adicionarHeaderLocation(response, identificador);
    }

    private void adicionarHeaderLocation(HttpServletResponse response, Object identificador) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{identificador}")
                .buildAndExpand(identificador).toUri();

        response.setHeader("Location", uri.toASCIIString());

    }
}
