package br.ufg.pos.fswb.mymoneyapi.base.resource.exceptions.handler;

import br.ufg.pos.fswb.mymoneyapi.base.resource.exceptions.dto.ErroDTO;
import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.AppGenericException;
import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.NaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(value = {AppGenericException.class})
    public ResponseEntity<Object> handlerAppGenericException(AppGenericException ex, WebRequest request) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

        return getObjectResponseEntity(ex, request, httpStatus);
    }

    @ExceptionHandler(value = {NaoEncontradoException.class})
    public ResponseEntity<Object> handlerNaoEncontradoException(NaoEncontradoException ex, WebRequest request) {
        return getObjectResponseEntity(ex, request, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<Object> getObjectResponseEntity(AppGenericException ex, WebRequest request, HttpStatus httpStatus) {
        final String motivo = messageSource.getMessage(ex.getMotivo(), null, LocaleContextHolder.getLocale());
        final String detalhe = messageSource.getMessage(ex.getDetalhe(), ex.getParams(), LocaleContextHolder.getLocale());

        final ErroDTO erro = new ErroDTO(motivo, detalhe, getRequestURI(request));
        return handleExceptionInternal(ex, Arrays.asList(erro), new HttpHeaders(), httpStatus, request);
    }

    private String getRequestURI(WebRequest request) {
        return ((ServletWebRequest) request).getRequest().getRequestURI();
    }
}
