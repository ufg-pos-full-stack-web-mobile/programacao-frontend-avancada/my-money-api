package br.ufg.pos.fswb.mymoneyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class MyMoneyApiApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MyMoneyApiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MyMoneyApiApplication.class);
	}

}
