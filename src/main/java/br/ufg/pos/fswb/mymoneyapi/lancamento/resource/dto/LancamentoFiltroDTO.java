package br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto;

import br.ufg.pos.fswb.mymoneyapi.base.util.DataUtil;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;

import java.math.BigDecimal;

public class LancamentoFiltroDTO {

    private Long diaInicial;
    private Long diaFinal;
    private Double valorInicial;
    private Double valorFinal;
    private Long idConta;

    public Long getDiaInicial() {
        return diaInicial;
    }

    public void setDiaInicial(Long diaInicial) {
        this.diaInicial = diaInicial;
    }

    public Long getDiaFinal() {
        return diaFinal;
    }

    public void setDiaFinal(Long diaFinal) {
        this.diaFinal = diaFinal;
    }

    public Double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public Double getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(Double valorFinal) {
        this.valorFinal = valorFinal;
    }

    public Long getIdConta() {
        return idConta;
    }

    public void setIdConta(Long idConta) {
        this.idConta = idConta;
    }

    public static class LancamentoFiltroDTOTransformer {
        public static LancamentoFiltro toFilter(LancamentoFiltroDTO filtroDto) {
            LancamentoFiltro filtro = new LancamentoFiltro();
            if (filtroDto.getDiaInicial() != null) {
                filtro.setDataInicial(DataUtil.fromTimestamp(filtroDto.getDiaInicial()));
            }
            if (filtroDto.getDiaFinal() != null) {
                filtro.setDataFinal(DataUtil.fromTimestamp(filtroDto.getDiaFinal()));
            }
            if (filtroDto.getValorInicial() != null) {
                filtro.setValorInicial(BigDecimal.valueOf(filtroDto.getValorInicial()));
            }
            if (filtroDto.getValorFinal() != null) {
                filtro.setValorFinal(BigDecimal.valueOf(filtroDto.getValorFinal()));
            }
            if (filtroDto.getIdConta() != null) {
                final Conta conta = new Conta();
                conta.setId(filtroDto.getIdConta());
                filtro.setConta(conta);
            }
            return filtro;
        }
    }
}
