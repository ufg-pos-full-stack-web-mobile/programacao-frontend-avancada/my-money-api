package br.ufg.pos.fswb.mymoneyapi.base.resource.exceptions.dto;

public class ErroDTO {

    private String titulo;
    private String detalhe;
    private String source;

    public ErroDTO(String titulo, String detalhe, String pointer) {
        this.titulo = titulo;
        this.detalhe = detalhe;
        this.source = pointer;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
