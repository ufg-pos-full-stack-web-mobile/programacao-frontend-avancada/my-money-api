package br.ufg.pos.fswb.mymoneyapi.lancamento.resource.dto;

import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;

import java.time.ZoneOffset;

public class LancamentoDTO {

    private Long codigo;
    private Long dia;
    private Double valor;
    private String conta;
    private String tipo;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Long getDia() {
        return dia;
    }

    public void setDia(Long dia) {
        this.dia = dia;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public static class LancamentoDTOTransformer {
        public static LancamentoDTO toDto(Lancamento lancamento) {
            final LancamentoDTO dto = new LancamentoDTO();
            dto.setCodigo(lancamento.getId());
            dto.setDia(lancamento.getDia().atStartOfDay().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
            dto.setValor(lancamento.getValor().doubleValue());
            dto.setConta(lancamento.getConta().getNome());
            dto.setTipo(lancamento.getTipoLancamento().name());
            return dto;
        }
    }
}
