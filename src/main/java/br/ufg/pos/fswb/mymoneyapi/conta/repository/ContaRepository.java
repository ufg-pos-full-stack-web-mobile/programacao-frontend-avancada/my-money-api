package br.ufg.pos.fswb.mymoneyapi.conta.repository;

import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContaRepository extends JpaRepository<Conta, Long> {
}
