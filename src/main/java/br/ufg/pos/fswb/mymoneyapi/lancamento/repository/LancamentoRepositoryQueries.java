package br.ufg.pos.fswb.mymoneyapi.lancamento.repository;

import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;

import java.util.List;

public interface LancamentoRepositoryQueries {

    List<Lancamento> filtrar(LancamentoFiltro filtro);
}
