package br.ufg.pos.fswb.mymoneyapi.conta.services;

import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.NaoEncontradoException;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;

import java.util.List;

public interface ContaService {

    List<Conta> recuperarTodas();

    Conta salvar(Conta conta);

    Conta buscarPorIdentificador(Long codigo) throws NaoEncontradoException;

    Conta excluirPorCodigo(Long codigo);

    Conta atualizarSaldo(Lancamento lancamento);
}
