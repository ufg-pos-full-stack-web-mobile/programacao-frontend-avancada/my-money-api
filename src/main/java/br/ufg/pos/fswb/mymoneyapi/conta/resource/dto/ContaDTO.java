package br.ufg.pos.fswb.mymoneyapi.conta.resource.dto;

import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;

import java.math.BigDecimal;

public class ContaDTO {

    private Long codigo;
    private String nome;
    private double saldo;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public static final class ContaDTOTransformer {

        public static final ContaDTO toDto(Conta conta) {
            final ContaDTO dto = new ContaDTO();
            dto.setCodigo(conta.getId());
            dto.setNome(conta.getNome());
            dto.setSaldo(conta.getSaldo().doubleValue());
            return dto;
        }

        public static final Conta toEntity(ContaDTO contaDto) {
            final Conta conta = new Conta();
            conta.setNome(contaDto.getNome());
            conta.setSaldo(BigDecimal.valueOf(contaDto.getSaldo()));
            return conta;
        }
    }
}
