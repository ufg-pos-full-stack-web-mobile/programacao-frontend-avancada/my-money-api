package br.ufg.pos.fswb.mymoneyapi.lancamento.service.impl;

import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.NaoEncontradoException;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.conta.services.ContaService;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.LancamentoRepository;
import br.ufg.pos.fswb.mymoneyapi.lancamento.repository.filter.LancamentoFiltro;
import br.ufg.pos.fswb.mymoneyapi.lancamento.service.LancamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class LancamentoServiceImpl implements LancamentoService {

    private final LancamentoRepository repository;
    private final ContaService contaService;

    @Autowired
    public LancamentoServiceImpl(LancamentoRepository repository, @Lazy ContaService contaService) {
        this.repository = repository;
        this.contaService = contaService;
    }

    @Override
    public List<Lancamento> recuperarTodos() {
        return repository.findAll();
    }

    @Override
    public Lancamento salvar(Lancamento lancamento) {
        try {
            Conta conta = contaService.buscarPorIdentificador(lancamento.getConta().getId());
            lancamento.setConta(conta);
            Lancamento saved = repository.save(lancamento);
            contaService.atualizarSaldo(lancamento);
            return saved;
        } catch (NaoEncontradoException e) {
            // TRATAR DEPOIS
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Lancamento> filtrar(LancamentoFiltro lancamentoFiltro) {
        return repository.filtrar(lancamentoFiltro);
    }
}
