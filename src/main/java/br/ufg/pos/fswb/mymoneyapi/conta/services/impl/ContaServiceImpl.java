package br.ufg.pos.fswb.mymoneyapi.conta.services.impl;

import br.ufg.pos.fswb.mymoneyapi.base.services.exceptions.NaoEncontradoException;
import br.ufg.pos.fswb.mymoneyapi.conta.model.Conta;
import br.ufg.pos.fswb.mymoneyapi.conta.repository.ContaRepository;
import br.ufg.pos.fswb.mymoneyapi.conta.services.ContaService;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import br.ufg.pos.fswb.mymoneyapi.lancamento.model.TipoLancamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class ContaServiceImpl implements ContaService {

    private final ContaRepository contaRepository;

    @Autowired
    public ContaServiceImpl(ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }

    @Override
    public List<Conta> recuperarTodas() {
        return contaRepository.findAll();
    }

    @Override
    public Conta salvar(Conta conta) {
        return contaRepository.save(conta);
    }

    @Override
    public Conta buscarPorIdentificador(Long codigo) throws NaoEncontradoException {
        Optional<Conta> optional = contaRepository.findById(codigo);
        return optional.orElseThrow(() -> new NaoEncontradoException(codigo));
    }

    @Override
    public Conta excluirPorCodigo(Long codigo) {
        final Conta conta = contaRepository.getOne(codigo);
        contaRepository.delete(conta);
        return conta;
    }

    @Override
    public Conta atualizarSaldo(Lancamento lancamento) {
        Conta conta = lancamento.getConta();
        Optional<Conta> optional = contaRepository.findById(conta.getId());
        conta = optional.get();

        if (lancamento.getTipoLancamento() == TipoLancamento.SAIDA) {
            conta.setSaldo(conta.getSaldo().subtract(lancamento.getValor()));
        } else {
            conta.setSaldo(conta.getSaldo().add(lancamento.getValor()));
        }
        contaRepository.save(conta);
        return conta;
    }
}
