package br.ufg.pos.fswb.mymoneyapi.base.util;

import java.time.*;

public final class DataUtil {

    public static final LocalDate fromTimestamp(Long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        return dateTime.toLocalDate();
    }

    public static final Long getLongMili(int year, Month month, int dayOfMonth) {
        return LocalDate.of(year, month, dayOfMonth).atStartOfDay().atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
    }
}
