package br.ufg.pos.fswb.mymoneyapi.lancamento.model;

public enum TipoLancamento {

    ENTRADA,
    SAIDA;
}
