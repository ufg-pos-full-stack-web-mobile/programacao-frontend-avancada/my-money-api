package br.ufg.pos.fswb.mymoneyapi.base.services.exceptions;

public abstract class AppGenericException extends Exception {

    private final String motivo;
    private final String detalhe;
    private final Object[] params;

    public AppGenericException(String motivo, String detalhe, Object... params) {
        this.motivo = motivo;
        this.detalhe = detalhe;
        this.params = params;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public Object[] getParams() {
        return params;
    }

}
