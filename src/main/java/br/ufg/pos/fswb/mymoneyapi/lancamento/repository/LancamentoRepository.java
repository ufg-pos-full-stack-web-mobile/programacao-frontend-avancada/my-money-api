package br.ufg.pos.fswb.mymoneyapi.lancamento.repository;

import br.ufg.pos.fswb.mymoneyapi.lancamento.model.Lancamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQueries {
}
