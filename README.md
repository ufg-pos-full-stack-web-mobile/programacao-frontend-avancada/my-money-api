# My Money API

Servidor de Back-end para o projeto My Money

## Compilando o projeto

Para compilar o projeto é preciso ter o JDK na versão 8 instalado na máquina.

Com o JDK instalado e configurado, faça o clone do projeto.

Vá na linha de comando, no diretório do projeto clonado e execute o seguinte comando:

WINDOWS
``` 
$ mvnw.cmd clean install

```

LINUX
```
./mvnw clean install
```

Pegue o projeto compilado dentro da pasta _target_ que será gerada pelo Maven.'
